console.log('_______________');
console.log('soal 1');

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var hewan = daftarHewan.sort();
for (let i = 0; i < hewan.length; i++) {
    console.log(hewan[i]);
}
console.log('_______________');


// soal 2
console.log('soal 2');
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };
introduce = () => {
    this.data = data;
    return console.log(`Nama saya ${ data.name }, umur saya ${ data.age } tahun, alamat saya di ${ data.address }, dan saya punya hobby yaitu ${ data.hobby }`);
}

introduce();
console.log('_______________');



// soal 3

console.log('soal 3');
var hitung_1 = "Muhammad";

var hitung_2 = "Iqbal";

function hitungHurufVokal(str) {
    var vowelsCount = 0;
    for (i in str) {
        switch (str[i]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'A':
            case 'I':
            case 'U':
            case 'E':
            case 'O':

                vowelsCount++;
                break;
        }
    }
    return vowelsCount;

}

console.log(hitungHurufVokal(hitung_1));
console.log(hitungHurufVokal(hitung_2));
console.log('_______________');

// soal 4
console.log('soal 4');

hitung = (angka) => {
    var asal = -2;
    for (i = 0; angka > i; i++) {

        asal += 2;

    }
    return asal;
}

console.log(hitung(4));
console.log('_______________');